# Pycharm IDE
# This is the program to import the dataset for our model.


import os


# remove data in the import folder in Neo4j configuration files 
# in our virtual machine, the path of the import folder is /var/lib/neo4j/import
# if you are not on the same environment than us, please verify the location of the import folder at https://neo4j.com/docs/operations-manual/current/configuration/file-locations/ 
passWord ='Activus2020' # enter the password of your user account to authorize a command system in the virtual machine
cmd = 'sudo rm /var/lib/neo4j/import/*.csv'

# os.system(cmd) execute the command as in the terminal
# enter the password automatically
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# import csv files from the folder containing the dataset for our model to the import file of Neo4j
# in the first argument of cmd, specify the folder in which the dataset for our model are and add /*.csv
cmd = 'sudo cp /home/activus01/data/temporalgraph/*.csv /var/lib/neo4j/import'

os.system('echo %s|sudo -S %s' % (passWord, cmd))

# clean database dedicated for the dataset of our model
# here our database is called temporal_graph 
cmd = 'sudo rm /var/lib/neo4j/data/databases/temporalgraph.db/*'
os.system('echo %s|sudo -S %s' % (passWord, cmd))

# stop neo4j server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j stop'
os.system('echo %s|sudo -S %s' % (passWord, cmd))


# import data 
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j-admin import --database=temporalgraph.db ' \
      '--nodes=/var/lib/neo4j/import/store.csv ' \
      '--nodes=/var/lib/neo4j/import/customer.csv ' \
      '--nodes=/var/lib/neo4j/import/income_band.csv ' \
      '--nodes=/var/lib/neo4j/import/item.csv ' \
      '--relationships=/var/lib/neo4j/import/has_household_demographics.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_customer.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_item.csv ' \
      '--relationships=/var/lib/neo4j/import/ss_customer_item.csv ' \
      '--skip-bad-relationships --skip-duplicate-nodes'


os.system('echo %s|sudo -S %s' % (passWord, cmd))

# start server
cmd = 'cd /var/lib/neo4j/'
os.system(cmd)
cmd = 'sudo neo4j start'
os.system('echo %s|sudo -S %s' % (passWord, cmd))
