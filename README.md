**Experimental evaluation in the article "Towards an efficient approach to manage graph data evolution: conceptual modelling and experimental assessments" (RCIS 2021)**


/!\ The personal information of this gitlab account is temporarily anonymous during the process review of the article in RCIS 2021. It is not possible to contact this account until the end of the process review. /!\


***Abstract of the paper***

This paper describes a new temporal graph modelling solution to organize and memorize changes in a business application. To
do so, we enrich the basic graph by adding the concepts of states and instances. Our model has first the advantage of representing a complete
temporal evolution of the graph, at the level of: (i) the graph structure, (ii) the attribute set of entities/relationships and (iii) the attributes’
value of entities/relationships. Then, it has the advantage of memorizing in an optimal manner evolution traces of the graph and retrieving easily
temporal information about a graph component. To validate the feasibility of our proposal, we implement our proposal in Neo4j, a data store
based on property graph model. We then compare its performance in terms of storage and querying time to the classical modelling approach
of temporal graph. Our results show that our model outperforms the classical approach by reducing disk usage by 12 times and saving up to
99% queries’ runtime.

***Summary of the experimental evaluation***

We present an experimental comparison of three approaches for modelling an evolving graph: the classical sequence of snapshots, an optimized
sequence of snapshots and our temporal graph. As mentioned in Section 2 of the paper, the classical snapshots approach consists in sampling graph data at a regular time period (here we have chosen a month). Our optimized snapshot approach consists in creating snapshots only if they differ. In other terms, we create a snapshot only if it includes a change compared to a previous snapshot.

This experiment has two objectives: (i) to study the feasibility of our model, i.e. illustrate if our modelling is easily implementable (stored and queried) in a graph-oriented data store and (ii) to study the efficiency of our model by comparing
its storage and query performance to the classical sequence of snapshots and the optimized sequence of snapshots

For this experiment, we built 3 datasets that includes the three temporal evolution components in our model based on the well-known benchmark TPC-DS ( http://www.tpc.org/tpc_documents_current_versions/pdf/tpc-ds_v2.13.0.pdf): (i) one dataset having the temporal graph representation, (ii) one dataset having the classical snapshots representation and (iii) one dataset having the optimized snapshots representation. Then, we stored each dataset in one instance of the Neo4j graph database management systems using the translation rules we defined. Finally, we queried the three approaches according to different querying criteria and compare their performance. 

***Technical environment***

The hardware configuration is as follows: PowerEdge R630, 16 CPUs x Intel(R) Xeon(R) CPU E5-2630 v3 @ 2.40Ghz, 63.91 GB. One virtual machine is installed on this hardware. This virtual machine (LINUX) has 6GB in terms of RAM and 100GB in terms of disk size. On the virtual machine, we installed the database management system **Neo4j Server** (the version **4.1.3** and the edition **Community**) as well as the IDE **Pycharm** (version **2020.1.2**).

_Install your technical environment:_
1) Install Neo4j Server.
2) Install Pycharm.
3) Open Pycharm.
4) Clone this remote repository in Pycharm. It will create a pycharm project. 

We also used SQL lite in one step of the datasets transformation.


***Generation of TPC-DS dataset***

The TPC-DS benchmark allows to generate datasets according to parameterized data volumes. We generated 1 dataset according to 1 scale factor:

- get the generator toolkit : download it from the [TPC-DS website](http://tpc.org/tpc_documents_current_versions/download_programs/tools-download-request5.asp?bm_type=TPC-DS&bm_vers=2.13.0&mode=CURRENT-ONLY) or download it the [v2.13.0rc1_.zip](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/v2.13.0rc1_.zip) of this project.
- follow the guidelines to use the the generator toolkit [here](https://datacadamia.com/data/type/relation/benchmark/tpcds/load#data_generation). We used the scale factor 1 to generate 1GB.
- get the generated data [here](https://filesender.renater.fr/?s=download&token=33a54faf-3f7a-48d4-9d7f-19a6844d9108).


***Transformation steps of TPC-DS dataset***

We extracted a subset of the TPC-DS dataset containing data over 5 years (from the year 2000 to 2005). Moreover, this subset contains dimension tables divided into entity types (STORE, ITEM, CUSTOMER, INCOME_BAND and HOUSEHOLD_DEMOGRAPHICS) and a fact table (STORE_SALES) as illusrated [here](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/subgraph_tpds.PNG). To be able to test the feasibility of our model, we transformed this dataset into a temporal graph representation. To do so, we applied the following transformation steps on the TPC-DS dataset:

**Step 1** In TPC-DS dataset, an entity is represented by several rows in a dimension table. Each row of a dimension table represents an entity with its attributes (in columns). Among those attributes, there are a start valid time and an end valid time depicting the time interval at which the attributes' value of the entity are valid. We therefore notice that the attributes' value of an entity evolve over time. In other terms, each row of a dimension table corresponds to an instance of an entity in our model, and a node with attributes in the graph environment.
    
**Step 2** Each row of the fact table STORE_SALES represents several relationships with quantitative attributes between several entities at a specific time instant. We extracted from each row of STORE_SALES the possible combinations of relationship between two entities to create new rows representing 1 to 1 relationships. Then, we aggregated the attributes' values and the time occurrence of each relationship to get its valid time interval. Each relationship resulting from this transformation step is an instance of a relationship in our model, and an edge with attributes in the graph environment. As a result, we created three relationship tables: SS_ITEM (This relationship type describes an item sold by a store.), SS_CUSTOMER_ITEM (This relationship type describes an item bought by a customer in a store.) and SS_CUSTOMER (This relationship type describes a client that shopped in a store.).

**Step 3** Entities and relationships at this step do not carry the evolution in schema (or the evolution in the attributes' set). We identified the evolution of schema of entities in the dimension tables  STORE and ITEM by using existing attributes of each entity and adding them progressively on instances according to the time intervals of their appearance (that we defined arbitrarily). Then, we introduced the evolution of schema of relationships using the same mechanism. We used the table HOUSEHOLD_DEMOGRAPHICS to create the relationship type HAS_HOUSEHOLD_DEMOGRAPHICS (This relationship type describes the category of income a customer belongs to, according to his household demographics.) between CUSTOMER AND INCOME_BAND that evolves in terms of attributes' value and schema.


To study the advantages of using our model, we compare it to two approaches of sequence of snapshots: the classical snapshot approach and an optimized snapshot approach. Therefore, we created two other datasets: a dataset with the classical snapshot-based representation (DS2) and a dataset with the optimized snapshot-based representation (DS3). To create DS2, we applied the following transformation steps on the TPC-DS dataset:

**Step 4** We duplicated each row of each dimension table (STORE, ITEM, CUSTOMER and INCOME_BAND) as the number of months in the time interval of the row. Moreover, we assigned to each row the attribute valid time which corresponds to the month of its validity. As a result of this transformation step, we obtained the snapshots of each entity at each month. 
    
**Step 5** We applied the Steps 2 and 3. Then, we duplicated each created row of each relationship table in DS1 (SS_ITEM, SS_CUSTOMER_ITEM, SS_CUSTOMER and HAS_HOUSEHOLD_DEMOGRAPHICS) as the number of months in the valid time interval. We assigned to each row the attribute valid time which corresponds to the month  of its validity. As a result of this transformation step, we obtained the snapshots of each relationship at each month. 

To create DS3, we applied the following transformation steps on DS2: 

**Step 6** We extracted from DS2 only the snapshots in which there are changes. This allows to reduce the data redundancy, in other terms, data that do not change over time.


***Implementation of the transformation steps of TPC-DS dataset***

We applied the transformation steps  1 - 2 - 3 using python programs (.py) in the folder [tpc_ds_transformation](https://gitlab.com/2573869/temporal_graph_modelling/-/tree/master/tpc_ds_transformation) as illustrated [here](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/tpc_ds_transformation/transformation_to_temporal_graph.PNG).  


We applied the transformation steps 4 - 5 using python programs (.py) in the folder [tpc_ds_transformation](https://gitlab.com/2573869/temporal_graph_modelling/-/tree/master/tpc_ds_transformation) as illustrated [here](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/tpc_ds_transformation/transformation_to_snapshots.PNG).  

We applied the transformation steps 6 using python programs (.py) in the folder [tpc_ds_transformation](https://gitlab.com/2573869/temporal_graph_modelling/-/tree/master/tpc_ds_transformation) as illustrated [here](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/tpc_ds_transformation/transformation_to_snapshotsV2.PNG).


***Donwload transformed datasets***

1) Download the datasets we use in our experiment [here](https://cloud.irit.fr/index.php/s/wui9hA25djRQnBM). The dowloaded folder contains three folders: one for the data structured according to our model (called _**temporalgraph**_), one for the data structured according to classical snapshots approach (called _**snapshots**_) and one structured according to the optimized approach (called _**snapshotsv2**_). Each folder contains 8 csv files (customer, has_household_demographics, income_band, item, ss_customer, ss_customer_item, ss_item, store).

2) Save the datasets in a local directory. 

***Import datasets in Neo4j*** 

Open the cloned project in Pycharm. Make sure to follow the guidelines in a file.py before running it, some changes need to be done on some variables.

1) Run the file  [_**import_temporal_graph.py**_](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/querytemporalgraph.py) to import the dataset for our model.
2) Run the file  [_**import_snapshots.py**_](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/import_snapshots.py)  to import the dataset for the classical snapshots approach.
3) Run the file   [_**import_snapshotsv2.py**_](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/import_snapshotsv2.py)  to import the dataset for the optimized snapshots approach.


***Queries***

We present in the file [benchmark_queries](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/benchmark_queries) the queries we used as well as their business meaning. To run the 28 queries and get their runtime, open the cloned project in Pycharm. Make sure to follow the guidelines in a file.py before running it, some changes need to be done on some variables.

1) Run the file  [_**querysnapshots.py**_](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/querysnapshots.py). It will produce a csv file with the runtime of each query for the classical snapshots approach.
2) Run the file  [_**querysnapshotsV2.py**_](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/querysnapshotsv2.py). It will produce a csv file with the runtime of each query for the optimized snapshots approach.
3) Run the file  [_**querytemporalgraph.py**_](https://gitlab.com/2573869/temporal_graph_modelling/-/blob/master/querytemporalgraph.py). It will produce a csv file with the runtime of each query for the temporal graph approach.

