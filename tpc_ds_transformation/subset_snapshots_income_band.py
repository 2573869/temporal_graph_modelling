

###Extract the data subset starting from the year 2000 to 2005
## create snapshots of income_band
#modify the input_file and output_file variable for each transformation.

import pandas as pd
import datetime as dt


# list of month between two dates
def get_list_months(start_date, end_date):
    """

    :param start_date: the start valid time
    :param end_date: the end valid time
    :return: the list of month between the two times. We do not include the end valid time in this list because it is the valid time of the next instance.
    """
    return pd.date_range(start_date, end_date, freq='M').strftime('%Y-%m').tolist()


# + pd.offsets.MonthEnd() to include the end_date https://stackoverflow.com/questions/37890391/how-to-include-end-date-in-pandas-date-range-method

def convert_date_yearmonth(date):
    """

    :param date: must be in date format
    :return: date in string
    """
    res = dt.datetime.strftime(date, '%Y-%m')
    return res


def get_list_months_dataset():  # 10 years of data
    # 1997-03-13: minimum start_valid_time
    # 2007-03-04: maximum
    # choice of 5 years = 72 months
    return pd.date_range("2000-01-01", "2006-01-01", freq='M').strftime('%Y-%m').tolist()


import pandas as pd


def create_csv_income_band_clean(output_file):
    """
    :param output_file: the path of the output file
    :return: return the csv into a dataframe form
    """
    dfResult = pd.DataFrame(
        columns=['income_band_sk:ID(Income_Band)', 'income_band_id', 'lower_band', 'upper_band', 'valid_time',
                 ':LABEL'])
    dfResult.to_csv(output_file, index=False, sep=',')
    return dfResult


def add_row_income_band_clean(output_file, sk, row, new_date):
    """
    :param output_file: csv file that record the transformed data => store_clean.csv
    :param sk: store_sk
    :param row: the row of the input file that is copied to the output file
    :param date: the new valid time of the row
    :return:
    """

    add_line = pd.DataFrame(
        columns=['income_band_sk:ID(Income_Band)', 'income_band_id', 'lower_band', 'upper_band', 'valid_time',
                 ':LABEL'])
    add_line = add_line.append(
        {'income_band_sk:ID(Income_Band)': sk, 'income_band_id': row['income_band_id'], 'lower_band': row['lower_band'],
         'upper_band': row['upper_band'], 'valid_time': new_date, ':LABEL': row[':LABEL']}, ignore_index=True)
    add_line.to_csv(output_file, mode='a', header=False, index=False,
                    sep=',')  # mode='a' means append, header=False means not copy the header at each addition of a row, index=False means not add automatically an index to each row



#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/income_band.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/income_band.csv'


#read the input file as a dataframe
df_input = pd.read_table(input_file, sep=",")
print(df_input.columns)


df_input['income_band_id'] = df_input['income_band_sk:ID(Income_Band)']

#create the output file
df_output=create_csv_income_band_clean(output_file)
print(df_output.columns)


#get the time of the transformation
time_start = time.time()
#fill the output file with transformed data
sk=0
for index, row in df_input.iterrows():
       for m in get_list_months_dataset():
            sk=sk+1
            add_row_income_band_clean(output_file, sk, row, m)
time_end=time.time()
time_run = time_end-time_start
print("It takes" + str(time_run) + "to transform income_band.csv!")


