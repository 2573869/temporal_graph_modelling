###Extract the data  subset starting from the year 2000 to 2005
#create snapshots of ss_customer_item
#modify the input_file and output_file variable for each transformation.
import pandas as pd
import time
import datetime as dt
import numpy as np


# list of month between two dates
def get_list_months(start_date, end_date):
    """

    :param start_date: the start valid time
    :param end_date: the end valid time
    :return: the list of month between the two times. We do not include the end valid time in this list because it is the valid time of the next instance.
    """
    return pd.date_range(start_date, end_date, freq='M').strftime('%Y-%m').tolist()


# + pd.offsets.MonthEnd() to include the end_date https://stackoverflow.com/questions/37890391/how-to-include-end-date-in-pandas-date-range-method

def convert_date_yearmonth(date):
    """

    :param date: must be in date format
    :return: date in string
    """
    res = dt.datetime.strftime(date, '%Y-%m')
    return res


def get_list_months_dataset():  # 10 years of data
    # 1997-03-13: minimum start_valid_time
    # 2007-03-04: maximum
    # choice of 5 years = 72 months
    return pd.date_range("2000-01-01", "2006-01-01", freq='M').strftime('%Y-%m').tolist()


def create_ss_customer_item_inter(output_inter):
    """
    :param output_file: the path of the output file
    :return: return the csv into a dataframe form
    """
    dfResult = pd.DataFrame(columns=[':START_ID(Customer)', 'customer_sk', 'customer_id',
                                     'item_sk', 'item_id', 'net_paid', 'number_of_buy_times',
                                     'valid_time', ':END_ID(Item)', ':TYPE'])
    dfResult.to_csv(output_file, index=False, sep=',')
    return dfResult


def add_row_ss_customer_item_inter(output_inter, row, new_date):
    """
    :param output_file: csv file that record the transformed data
    :param row: the row of the input file that is copied to the output file
    :param date: the new valid time of the row
    :return:
    """

    add_line = pd.DataFrame(columns=[':START_ID(Customer)', 'customer_sk', 'customer_id',
                                     'item_sk', 'item_id', 'net_paid', 'number_of_buy_times',
                                     'valid_time', ':END_ID(Item)', ':TYPE'])
    add_line = add_line.append({':START_ID(Customer)': None, 'customer_sk': None, 'customer_id': row['customer_id'],
                                'item_sk': None, 'item_id': row['item_id'],
                                'net_paid': row['net_paid'], 'number_of_buy_times': row['number_of_buy_times'],
                                ':END_ID(Item)': None, ':TYPE': row[':TYPE'], 'valid_time': new_date},
                               ignore_index=True)
    add_line.to_csv(output_file, mode='a', header=False, index=False,
                    sep=',')  # mode='a' means append, header=False means not copy the header at each addition of a row, index=False means not add automatically an index to each row

#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/ss_customer_item.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/ss_customer_item_inter.csv'
customer_file = 'D:/SAC 2021/Data/RAW/1GB/customer.csv'
item_file = 'D:/SAC 2021/Data/RAW/1GB/item.csv'


#read the input file as a dataframe
df_ss_customer_item = pd.read_table(input_file, sep=",", parse_dates=["start_valid_time", "end_valid_time"])

df_customer = pd.read_table(customer_file, sep=",")
df_customer.rename(columns={'customer_sk:ID(Customer)':'customer_sk'}, inplace=True)

df_customer=df_customer[['customer_sk', 'customer_id']].copy()
#merge has_household_demographics with customer to get the customer_id
df_merge1 = df_ss_customer_item.merge(df_customer, on="customer_sk", how='left')

#transform the valid_times of the relations in months
#add a new column valid_time
df_merge1["valid_time"] = None

df_item = pd.read_table(item_file, sep=",")
df_item.rename(columns={'item_sk:ID(Item)':'item_sk'}, inplace=True)

df_item=df_item[['item_sk', 'item_id']].copy()
df_merge2 = df_merge1.merge(df_item, on="item_sk", how='left')

df_output_inter=create_ss_customer_item_inter(output_file)

time_start = time.time()
#fill the output file with transformed data

for index, row in df_merge2.iterrows():
    if str(row['end_valid_time'])>= "2000-01-01" or pd.isnull(row['end_valid_time']):
        if row['start_valid_time'] == row['end_valid_time']:
            # convert a date into year-month
            m = convert_date_yearmonth(row['start_valid_time'])
            #replace valid_time
            add_row_ss_customer_item_inter(output_file,row,m)
            #print("end_valid_time==start_valid_time")
        elif pd.isnull(row['end_valid_time']) == True :
            date = convert_date_yearmonth(row['start_valid_time'])
            #end_valid_time==Null means that the rest of the time the entity is valid
            list_month = [m for m in get_list_months_dataset() if m >= date]
            for m in list_month:
                #replace valid_time
                add_row_ss_customer_item_inter(output_file,row,m)
                #print("end_valid_time==null")
        elif row['start_valid_time'] != row['end_valid_time'] and pd.isnull(row['end_valid_time']) == False :
            list_month = [m for m in get_list_months(row['start_valid_time'], row['end_valid_time']) if m in get_list_months_dataset()]
            for m in list_month:
                add_row_ss_customer_item_inter(output_file,row,m)
                #print("end_valid_time=!start_valid_time")

time_end=time.time()
time_run = time_end-time_start
print("It takes " + str(time_run)+ " to transform insert ss_customer_item!")


# Create the final table of ss_customer_item

#read clean data
input_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/ss_customer_item_inter.csv'
output_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/ss_customer_item.csv'
customer_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/customer.csv'
item_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/item.csv'

#read the input file as a dataframe
df_ss_customer_item_clean = pd.read_table(input_clean_file, sep=",")

# read customer_clean

df_customer_clean = pd.read_table(customer_clean_file, sep=",")

df_customer_clean.rename(columns={'customer_sk:ID(Customer)':'customer_sk'}, inplace=True)
df_customer_clean=df_customer_clean[['customer_sk', 'customer_id', 'valid_time']].copy()

# merge ss_customer_clean and customer to get customer_sk
df_merge1 = df_ss_customer_item_clean.merge(df_customer_clean, on=["customer_id","valid_time"], how='left')
df_merge1[':START_ID(Customer)'] = df_merge1['customer_sk_y']
df_merge1=df_merge1[[':START_ID(Customer)', 'customer_id', 'item_sk',
       'item_id', 'net_paid', 'number_of_buy_times', 'valid_time',
       ':END_ID(Item)', ':TYPE', 'customer_sk_y']].copy()


df_merge1.rename(columns={'customer_sk_y':'customer_sk'}, inplace=True)

# Read item
df_item_clean = pd.read_table(item_clean_file, sep=",")
df_item_clean.rename(columns={'item_sk:ID(Item)':'item_sk'}, inplace=True)
df_item_clean=df_item_clean[['item_sk', 'item_id', 'valid_time']].copy()

# merge ss_customer_item_clean and item_clean to get item_sk
df_merge2 = df_merge1.merge(df_item_clean, on=["item_id","valid_time"], how='left')
df_merge2 = df_merge2[df_merge2['item_sk_y'].notna()]
df_merge2["item_sk_y"] = df_merge2["item_sk_y"].astype(np.int64)
df_merge2[':END_ID(Item)'] = df_merge2['item_sk_y']


df_merge2=df_merge2[[':START_ID(Customer)', 'customer_id','item_id',
       'net_paid', 'number_of_buy_times', 'valid_time', ':END_ID(Item)',
       ':TYPE', 'customer_sk', 'item_sk_y']].copy()


df_merge2.rename(columns={'item_sk_y':'item_sk'}, inplace=True)
df_ss_customer_item_final = df_merge2[[':START_ID(Customer)','customer_sk', 'item_sk', 'net_paid',
       'number_of_buy_times', 'valid_time', ':END_ID(Item)', ':TYPE']].copy()


df_ss_customer_item_final.to_csv(output_clean_file, header=True, index=False, sep=',')