#On my local machine

# this module allows to convert the original data of fact tables in the format .dat to .csv file to import them in SQL database (SQL LITE) where we will create relations

# You need to change the path_0 and path_1 to respectively read the .dat files and save the .csv files
import os
import pandas as pd
import function
import time
import numpy as np

time_start = time.time()
# where we find the data .dat
path_0 = '/Users/arslan/Desktop/data16GB/'

# where we create the csv
path_1 = '/Users/arslan/Desktop/data16GB/CSV/'

# transfer store_sales

#we have to rename manually the file "store_sales.dat" to "store_sales.csv" in order to allow pandas to process it because it is two large in volume
# os.path.join to concatenate two paths
dir_path = os.path.join(path_0, "store_sales.csv")
new_path = os.path.join(path_1, "store_sales.csv")
dir_path_date = os.path.join(path_0, "date_dim.dat")

# dataframe date
dfDate = pd.read_table(dir_path_date, sep="|", header=None)
dfDate = dfDate.iloc[:, [0, 2]]
dfDate.columns = ["date_sk", "date"]

df = function.readBigCSV(dir_path)
df = df.iloc[:, [0, 2, 3, 7, 8, 9, 10, 11, 12, 13, 19, 20, 21, 22]]
df.columns = ["date_sk", "item_sk", "customer_sk", "store_sk", "promo_sk", "ticket_number",
              "quantity", "wholesale_cost", "list_price", "sales_price", "coupon_amt", "net_paid",
              "net_paid_inc_tax", "net_profit"]

# change the type of columns sk with integer
df = df.fillna(0)
df['customer_sk'] = df['customer_sk'].astype(int)
df['item_sk'] = df['item_sk'].astype(int)
df['ticket_number'] = df['ticket_number'].astype(int)
df['store_sk'] = df['store_sk'].astype(int)
df['promo_sk'] = df['promo_sk'].astype(int)

# delete data where store_sk=0
df = df.drop(df[df.store_sk == 0].index)
df = df.drop(df[df.customer_sk == 0].index)

#convert date in user view
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'sold_date_sk'}, inplace=True)

df['sold_date_sk'] = pd.to_datetime(df['sold_date_sk'])

df.to_csv(new_path, index=False, sep=",")


# transfer store_returns
dir_path = os.path.join(path_0, "store_returns.csv")
new_path = os.path.join(path_1, "store_returns.csv")

df = function.readBigCSV(dir_path)

df = df.iloc[:, [0, 2, 3, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]]
df.columns = ["date_sk", "item_sk", "customer_sk", "store_sk", "reason_sk", "ticket_number",
              "return_quantity", "return_amt", "return_tax", "return_amt_inc_tax", "return_fee", "return_ship_cost",
              "refunded_cash", "returned_charge", "store_credit", "net_loss"]

df = df.fillna(0)
df['ticket_number'] = df['ticket_number'].astype(int)
df['customer_sk'] = df['customer_sk'].astype(int)
df['item_sk'] = df['item_sk'].astype(int)
df['store_sk'] = df['store_sk'].astype(int)
df['reason_sk'] = df['reason_sk'].astype(int)

# delete data where store_sk=0
df = df.drop(df[df.store_sk == 0].index)
df = df.drop(df[df.customer_sk == 0].index)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'returned_date_sk'}, inplace=True)

df.to_csv(new_path, index=False, sep=",")

# transfer web_sales
dir_path = os.path.join(path_0, "web_sales.csv")
new_path = os.path.join(path_1, "web_sales.csv")

df = function.readBigCSV(dir_path)

df = df.iloc[:, [0, 2, 3, 4, 8, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33]]
df.columns = ["date_sk", "ship_date_sk", "item_sk", "bill_customer_sk", "ship_customer_sk", "web_page_sk",
              "web_site_sk", "ship_mode_sk", "warehouse_sk", "promo_sk", "order_number",
              "quantity", "wholesale_cost", "list_price", "sales_price", "ext_discount_amt", "ext_sales_price",
              "ext_wholesale_cost", "ext_list_price", "ext_tax", "coupon_amt", "ext_ship_cost",
              "net_paid", "net_paid_inc_tax", "net_paid_inc_ship", "net_paid_inc_ship_tax", "net_profit"]

df = df.fillna(0)
df['order_number'] = df['order_number'].astype(int)
df['bill_customer_sk'] = df['bill_customer_sk'].astype(int)
df['item_sk'] = df['item_sk'].astype(int)
df['web_page_sk'] = df['web_page_sk'].astype(int)
df['web_site_sk'] = df['web_site_sk'].astype(int)
df['ship_mode_sk'] = df['ship_mode_sk'].astype(int)
df['warehouse_sk'] = df['warehouse_sk'].astype(int)
df['promo_sk'] = df['promo_sk'].astype(int)

# delete data where web_page_sk=0
df = df.drop(df[df.web_page_sk == 0].index)
df = df.drop(df[df.bill_customer_sk == 0].index)
df = df.drop(df[df.item_sk == 0].index)

df = df.fillna(0)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'sold_date_sk'}, inplace=True)
df.rename(columns={'ship_date_sk': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'ship_date_sk'}, inplace=True)

df.to_csv(new_path, index=False, sep=",")

# web returns
dir_path = os.path.join(path_0, "web_returns.csv")
new_path = os.path.join(path_1, "web_returns.csv")
df = function.readBigCSV(dir_path)

df = df.iloc[:, [0, 2, 3, 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]]
df.columns = ["date_sk", "item_sk", "refunded_customer_sk", "returning_customer_sk", "web_page_sk", "reason_sk",
              "order_number", "return_quantity", "return_amt", "return_tax", "return_amt_inc_tax", "return_fee",
              "return_ship_cost", "refunded_cash", "returned_charge", "account_credit", "net_loss"]
df = df.fillna(0)
df['order_number'] = df['order_number'].astype(int)
df['refunded_customer_sk'] = df['refunded_customer_sk'].astype(int)
df['item_sk'] = df['item_sk'].astype(int)
df['web_page_sk'] = df['web_page_sk'].astype(int)
df['reason_sk'] = df['reason_sk'].astype(int)

df = df.drop(df[df.web_page_sk == 0].index)
df = df.drop(df[df.refunded_customer_sk == 0].index)
df = df.drop(df[df.item_sk == 0].index)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'returned_date_sk'}, inplace=True)

df.to_csv(new_path, index=False, sep=",")

# transfer catalog_sales
dir_path = os.path.join(path_0, "catalog_sales.csv")
new_path = os.path.join(path_1, "catalog_sales.csv")
df = function.readBigCSV(dir_path)

df = df.iloc[:, [0, 2, 3, 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
                 31, 32, 33]]
df.columns = ["date_sk", "ship_date_sk", "bill_customer_sk", "ship_customer_sk", "call_center_sk",
              "catalog_page_sk", "ship_mode_sk", "warehouse_sk", "item_sk", "promo_sk", "order_number",
              "quantity", "wholesale_cost", "list_price", "sales_price", "ext_discount_amt", "ext_sales_price",
              "ext_wholesale_cost", "ext_list_price", "ext_tax", "coupon_amt", "ext_ship_cost",
              "net_paid", "net_paid_inc_tax", "net_paid_inc_ship", "net_paid_inc_ship_tax", "net_profit"]

df = df.fillna(0)
df['order_number'] = df['order_number'].astype(int)
df['item_sk'] = df['item_sk'].astype(int)
df['call_center_sk'] = df['call_center_sk'].astype(int)
df['bill_customer_sk'] = df['bill_customer_sk'].astype(int)
df['catalog_page_sk'] = df['catalog_page_sk'].astype(int)
df['promo_sk'] = df['promo_sk'].astype(int)

df = df.drop(df[df.catalog_page_sk == 0].index)
df = df.drop(df[df.bill_customer_sk == 0].index)
df = df.drop(df[df.item_sk == 0].index)

df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'sold_date_sk'}, inplace=True)
df.rename(columns={'ship_date_sk': 'date_sk'}, inplace=True)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'ship_date_sk'}, inplace=True)

df.to_csv(new_path, index=False, sep=",")

# transfer catalog returns
dir_path = os.path.join(path_0, "catalog_returns.csv")
new_path = os.path.join(path_1, "catalog_returns.csv")
df = pd.read_table(dir_path, sep="|", header=None)

df = df.iloc[:, [0, 2, 3, 7, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26]]
df.columns = ["date_sk", "item_sk", "refunded_customer_sk", "returning_customer_sk", "call_center_sk",
              "catalog_page_sk", "ship_mode_sk", "warehouse_sk", "reason_sk", "order_number", "return_quantity",
              "return_amount", "return_tax", "return_amt_inc_tax", "fee", "return_ship_cost", "refunded_cash",
              "reversed_charge", "store_credit", "net_loss"]

df = df.fillna(0)
df['refunded_customer_sk'] = df['refunded_customer_sk'].astype(int)
df['item_sk'] = df['item_sk'].astype(int)
df['order_number'] = df['order_number'].astype(int)
df['call_center_sk'] = df['call_center_sk'].astype(int)
df['catalog_page_sk'] = df['catalog_page_sk'].astype(int)

df['cr_id:ID(Catalog_Returns)'] = "cr" + "-" + df['order_number'].map(str) + "-" + df['item_sk'].map(str)
df = pd.merge(df, dfDate, on="date_sk")
df = df.drop(columns=["date_sk"])
df.rename(columns={'date': 'returned_date_sk'}, inplace=True)

df.to_csv(new_path, index=False, sep=",")

time_end = time.time()
print('time cost', time_end - time_start, 's')
