# collect all the dates where there is a change in each csv file of the temporal graph


import pandas as pd
import datetime as dt
import time
from dateutil.parser import parse


def get_change_date(input_file):
    df_input = pd.read_table(input_file, sep=",")
    for index, row in df_input.iterrows():
        if str(row['start_valid_time'])< "2000-01-01":
            df_input.loc[index, 'start_valid_time'] = "2000-01-01"
    list_date = list(df_input.start_valid_time.astype(str).str[0:7].unique())
    return list_date

def add_new_dates_to_change_date(list_date):
    for m in list_date:
        if m not in change_date:
            change_date.append(m)



#create an empty list of change dates
change_date = []
change_date


#read raw data
input_file_store = 'D:/RCIS 2021/data/RAW/temporal_graph/store.csv'
input_file_item = 'D:/RCIS 2021/data/RAW/temporal_graph/item.csv'
input_file_ss_customer = 'D:/RCIS 2021/data/RAW/temporal_graph/ss_customer.csv'
input_file_ss_customer_item = 'D:/RCIS 2021/data/RAW/temporal_graph/ss_customer_item.csv'
input_file_ss_item = 'D:/RCIS 2021/data/RAW/temporal_graph/ss_item.csv'
input_file_has_household_demographics = 'D:/RCIS 2021/data/RAW/temporal_graph/has_household_demographics.csv'


## Store

list_store = get_change_date(input_file_store)

add_new_dates_to_change_date(list_store)

## Item
list_item = get_change_date(input_file_item)
add_new_dates_to_change_date(list_item)

## ss_customer
list_ss_customer = get_change_date(input_file_ss_customer)
add_new_dates_to_change_date(list_ss_customer)

## ss_customer_item
list_ss_customer_item = get_change_date(input_file_ss_customer_item)
add_new_dates_to_change_date(list_ss_customer_item)

## ss_item
list_ss_item = get_change_date(input_file_ss_item)
add_new_dates_to_change_date(list_ss_item)

## has_household_demographics
list_has_household_demographics = get_change_date(input_file_has_household_demographics)
add_new_dates_to_change_date(list_ss_customer_item)

# create the new snapshots
#read raw data in snapshots
input_file_snap_store = 'D:/RCIS 2021/data/RAW/snapshots/store.csv'
input_file_snap_item = 'D:/RCIS 2021/data/RAW/snapshots/item.csv'
input_file_snap_ss_customer = 'D:/RCIS 2021/data/RAW/snapshots/ss_customer.csv'
input_file_snap_ss_customer_item = 'D:/RCIS 2021/data/RAW/snapshots/ss_customer_item.csv'
input_file_snap_ss_item = 'D:/RCIS 2021/data/RAW/snapshots/ss_item.csv'
input_file_snap_has_household_demographics = 'D:/RCIS 2021/data/RAW/snapshots/has_household_demographics.csv'
input_file_snap_customer = 'D:/RCIS 2021/data/RAW/snapshots/customer.csv'
input_file_snap_income_band = 'D:/RCIS 2021/data/RAW/snapshots/income_band.csv'


#output file
output_file_snap_store = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/store.csv'
output_file_snap_item = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/item.csv'
output_file_snap_ss_customer = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/ss_customer.csv'
output_file_snap_ss_customer_item = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/ss_customer_item.csv'
output_file_snap_ss_item = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/ss_item.csv'
output_file_snap_has_household_demographics = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/has_household_demographics.csv'
output_file_snap_customer = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/customer.csv'
output_file_snap_income_band = 'D:/RCIS 2021/data/CLEAN/snapshotsV2/income_band.csv'

#copy paste change_date and delete nan
change_date = ['2000-01', '2000-03', '2001-03', '2002-03', '2002-04', '2002-05', '2003-05', '2003-06', '2003-07', '2004-07', '2003-03', '2003-04', '2004-04', '2004-05', '2004-06', '2005-06', '2000-10', '2001-10', '2002-10', '2003-10', '2003-11', '2004-11', '2004-12', '2005-12', '2006-01', '2001-12', '2001-08', '2000-12', '2000-06', '2000-04', '2002-02', '2002-11', '2002-12', '2001-07', '2000-07', '2001-06', '2002-09', '2000-11', '2001-04', '2001-11', '2001-01', '2000-09', '2000-02', '2002-07', '2000-08', '2001-05', '2001-02', '2002-06', '2002-01', '2000-05', '2002-08', '2003-01', '2001-09']

change_date.sort()

## Select rows from snapshots tables whose column value is in change_date
https://www.interviewqs.com/ddi-code-snippets/rows-cols-python

## Store

df_input_store = pd.read_table(input_file_snap_store, sep=",")
indexNames_store = df_input_store[ df_input_store['valid_time'].isin(change_date)==False ].index
df_input_store.drop(indexNames_store , inplace=True)
df_input_store.to_csv(output_file_snap_store, index=False, sep=',',  header=True)

## Item
df_input_item = pd.read_table(input_file_snap_item, sep=",")
indexNames_item = df_input_item[ df_input_item['valid_time'].isin(change_date)==False ].index
df_input_item.drop(indexNames_item , inplace=True)
df_input_item.to_csv(output_file_snap_item, index=False, sep=',',  header=True)

## ss_customer
df_input_ss_customer = pd.read_table(input_file_snap_ss_customer, sep=",")
indexNames_ss_customer = df_input_ss_customer[ df_input_ss_customer['valid_time'].isin(change_date)==False ].index
#no rows to drop

## ss_customer_item
df_input_ss_customer_item = pd.read_table(input_file_snap_ss_customer_item , sep=",")
indexNames_ss_customer_item = df_input_ss_customer_item[ df_input_ss_customer_item['valid_time'].isin(change_date)==False ].index
#no rows to drop

## ss_item
df_input_ss_item = pd.read_table(input_file_snap_ss_item , sep=",")
indexNames_ss_item = df_input_ss_item[ df_input_ss_item['valid_time'].isin(change_date)==False ].index
#no rows to drop

## has_household_demographics
df_input_hhd = pd.read_table(input_file_snap_has_household_demographics , sep=",")
indexNames_hhd = df_input_hhd[ df_input_hhd['valid_time'].isin(change_date)==False ].index
df_input_hhd.drop(indexNames_hhd , inplace=True)
df_input_hhd.to_csv(output_file_snap_has_household_demographics, index=False, sep=',',  header=True)

## Customer
df_input_customer = pd.read_table(input_file_snap_customer, sep=",")
indexNames_customer = df_input_customer[ df_input_customer['valid_time'].isin(change_date)==False ].index
df_input_customer.drop(indexNames_customer , inplace=True)
df_input_customer.to_csv(output_file_snap_customer, index=False, sep=',',  header=True)


## Income_band
df_input_income_band = pd.read_table(input_file_snap_income_band, sep=",")
indexNames_income_band = df_input_income_band[ df_input_income_band['valid_time'].isin(change_date)==False ].index
df_input_income_band.drop(indexNames_income_band , inplace=True)
df_input_income_band.to_csv(output_file_snap_income_band, index=False, sep=',',  header=True)
