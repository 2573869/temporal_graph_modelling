
import pandas as pd
import datetime as dt
import time
from dateutil.parser import parse

###Extract for each data table the subset starting from the year 2000 to 2005
#modify the input_file and output_file variable for each transformation.


####### STORE########
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/store.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/store.csv'
df_input = pd.read_table(input_file, sep=",")
indexNames = df_input[ (df_input['start_valid_time'] > '2006-01-01') | (df_input['end_valid_time'] < '2000-01-01') ].index
df_input.drop(indexNames , inplace=True)
df_input.loc[df_input['start_valid_time'] > '2006-01-01']
df_input.loc[df_input['end_valid_time'] < '2000-01-01']
df_input.to_csv(output_file, index=False, sep=',',  header=True)


######HAS_HOUSEHOLD_DEMORGAPHICS#######
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/has_household_demographics.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/has_household_demographics.csv'
df_input = pd.read_table(input_file, sep=",")
indexNames = df_input[ (df_input['start_valid_time'] > '2006-01-01') | (df_input['end_valid_time'] < '2000-01-01') ].index
df_input.drop(indexNames , inplace=True)
df_input.loc[df_input['start_valid_time'] > '2006-01-01']
df_input.loc[df_input['end_valid_time'] < '2000-01-01']
df_input.to_csv(output_file, index=False, sep=',',  header=True)


######ITEM#####
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/item.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/item.csv'
df_input = pd.read_table(input_file, sep=",")
indexNames = df_input[ (df_input['start_valid_time'] > '2006-01-01') | (df_input['end_valid_time'] < '2000-01-01') ].index
df_input.drop(indexNames , inplace=True)
df_input.loc[df_input['start_valid_time'] > '2006-01-01']
df_input.loc[df_input['end_valid_time'] < '2000-01-01']
df_input.to_csv(output_file, index=False, sep=',',  header=True)

###SS_CUSTOMER####
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/ss_customer.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/ss_customer.csv'
df_input = pd.read_table(input_file, sep=",")
indexNames = df_input[ (df_input['start_valid_time'] > '2006-01-01') | (df_input['end_valid_time'] < '2000-01-01') ].index
df_input.drop(indexNames , inplace=True)
df_input.loc[df_input['start_valid_time'] > '2006-01-01']
df_input.loc[df_input['end_valid_time'] < '2000-01-01']
df_input.to_csv(output_file, index=False, sep=',',  header=True)

###SS_ITEM###
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/ss_item.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/ss_item.csv'
df_input = pd.read_table(input_file, sep=",")
indexNames = df_input[ (df_input['start_valid_time'] > '2006-01-01') | (df_input['end_valid_time'] < '2000-01-01') ].index
df_input.drop(indexNames , inplace=True)
df_input.loc[df_input['start_valid_time'] > '2006-01-01']
df_input.loc[df_input['end_valid_time'] < '2000-01-01']
df_input.to_csv(output_file, index=False, sep=',',  header=True)


##SS_CUSTOMER_ITEM###
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/ss_customer_item.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/ss_customer_item.csv'
df_input = pd.read_table(input_file, sep=",")
indexNames = df_input[ (df_input['start_valid_time'] > '2006-01-01') | (df_input['end_valid_time'] < '2000-01-01') ].index
df_input.drop(indexNames , inplace=True)
df_input.loc[df_input['start_valid_time'] > '2006-01-01']
df_input.loc[df_input['end_valid_time'] < '2000-01-01']
df_input.to_csv(output_file, index=False, sep=',',  header=True)

# Add start valid time and end_valid_time to income_band and customer

###INCOME_BAND###
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/income_band.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/income_band.csv'
df_input = pd.read_table(input_file, sep=",")
# add new columns start and end_valid_time
df_input["start_valid_time"]= "2000-01-01"
df_input["end_valid_time"]= None
df_input.to_csv(output_file, index=False, sep=',',  header=True)

###CUSTOMER###
#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/subgraphTG/customer.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/newsubgraphTG/customer.csv'
df_input = pd.read_table(input_file, sep=",")
df_input["start_valid_time"]= "2000-01-01"
df_input["end_valid_time"]= None
df_input.to_csv(output_file, index=False, sep=',',  header=True)

## Verify nan values in each file
#read raw data in temporal_graph
input_file_store = 'D:/RCIS 2021/data/RAW/temporal_graph/store.csv'
input_file_item = 'D:/RCIS 2021/data/RAW/temporal_graph/item.csv'
input_file_ss_customer = 'D:/RCIS 2021/data/RAW/temporal_graph/ss_customer.csv'
input_file_ss_customer_item = 'D:/RCIS 2021/data/RAW/temporal_graph/ss_customer_item.csv'
input_file_ss_item = 'D:/RCIS 2021/data/RAW/temporal_graph/ss_item.csv'
input_file_has_household_demographics = 'D:/RCIS 2021/data/RAW/temporal_graph/has_household_demographics.csv'
input_file_customer = 'D:/RCIS 2021/data/RAW/temporal_graph/customer.csv'
input_file_income_band = 'D:/RCIS 2021/data/RAW/temporal_graph/income_band.csv'

def verify(input_file):
    df_input = pd.read_table(input_file, sep=",")
    df_input.start_valid_time.isna().sum()
    return input_file, df_input.start_valid_time.isna().sum()

list_temporal_graph = [input_file_store,input_file_item,input_file_ss_customer,input_file_ss_customer_item, input_file_ss_item,input_file_has_household_demographics,input_file_customer,input_file_income_band]
for i in list_temporal_graph:
    print(verify(i))
