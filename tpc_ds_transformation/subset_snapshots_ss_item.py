###Extract the data subset starting from the year 2000 to 2005
## create snapshots of ss_item
#modify the input_file and output_file variable for each transformation.

import pandas as pd
import time
import datetime as dt


# list of month between two dates
def get_list_months(start_date, end_date):
    """

    :param start_date: the start valid time
    :param end_date: the end valid time
    :return: the list of month between the two times. We do not include the end valid time in this list because it is the valid time of the next instance.
    """
    return pd.date_range(start_date, end_date, freq='M').strftime('%Y-%m').tolist()


# + pd.offsets.MonthEnd() to include the end_date https://stackoverflow.com/questions/37890391/how-to-include-end-date-in-pandas-date-range-method

def convert_date_yearmonth(date):
    """

    :param date: must be in date format
    :return: date in string
    """
    res = dt.datetime.strftime(date, '%Y-%m')
    return res


def get_list_months_dataset():  # 10 years of data
    # 1997-03-13: minimum start_valid_time
    # 2007-03-04: maximum
    # choice of 5 years = 72 months
    return pd.date_range("2000-01-01", "2006-01-01", freq='M').strftime('%Y-%m').tolist()


def create_ss_item_inter(output_inter):
    """
    :param output_file: the path of the output file
    :return: return the csv into a dataframe form
    """
    dfResult = pd.DataFrame(columns=[':START_ID(Store)', 'store_sk', 'store_id', 'item_sk', 'item_id',
                                     'net_profit', 'quantity', 'wholesale', 'sales_price', 'valid_time',
                                     ':END_ID(Item)', ':TYPE'])
    dfResult.to_csv(output_file, index=False, sep=',')
    return dfResult


def add_row_ss_item_inter(output_inter, row, new_date):
    """
    :param output_file: csv file that record the transformed data
    :param row: the row of the input file that is copied to the output file
    :param date: the new valid time of the row
    :return:
    """

    add_line = pd.DataFrame(columns=[':START_ID(Store)', 'store_sk', 'store_id', 'item_sk', 'item_id',
                                     'net_profit', 'quantity', 'wholesale', 'sales_price', 'valid_time',
                                     ':END_ID(Item)', ':TYPE'])
    add_line = add_line.append({':START_ID(Store)': None, 'store_sk': None, 'store_id': row['store_id'],
                                'item_sk': None, 'item_id': row['item_id'], 'net_profit': row['net_profit'],
                                'quantity': row['quantity'],
                                'wholesale': row['wholesale'], 'sales_price': row['sales_price'],
                                ':END_ID(Item)': None, ':TYPE': row[':TYPE'], 'valid_time': new_date},
                               ignore_index=True)
    add_line.to_csv(output_file, mode='a', header=False, index=False,
                    sep=',')  # mode='a' means append, header=False means not copy the header at each addition of a row, index=False means not add automatically an index to each row


# Intermediary table to transform ss_item
## get the id of store and item for each row in ss_item

#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/ss_item.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/ss_item_inter.csv'
item_file = 'D:/SAC 2021/Data/RAW/1GB/item.csv'
store_file = 'D:/SAC 2021/Data/RAW/1GB/store.csv'


#read the input file as a dataframe
df_ss_item = pd.read_table(input_file, sep=",", parse_dates=["start_valid_time", "end_valid_time"])

df_item = pd.read_table(item_file, sep=",")
df_item.rename(columns={'item_sk:ID(Item)':'item_sk'}, inplace=True)
df_item=df_item[['item_sk', 'item_id']].copy()
#merge has_household_demographics with customer to get the customer_id
df_merge1 = df_ss_item.merge(df_item, on="item_sk", how='left')

#transform the valid_times of the relations in months
#add a new column valid_time
df_merge1["valid_time"] = None


df_store = pd.read_table(store_file, sep=",")
df_store.rename(columns={'store_sk:ID(Store)':'store_sk'}, inplace=True)

df_store=df_store[['store_sk', 'store_id']].copy()
df_merge2 = df_merge1.merge(df_store, on="store_sk", how='left')
df_merge2.loc[df_merge2['item_id'] == 'AAAAAAAAGCHCAAAA']

df_item = pd.read_table(item_file, sep=",")

df_item.loc[df_item['item_id'] == 'AAAAAAAAGCHCAAAA']

df_output_inter=create_ss_item_inter(output_file)
time_start = time.time()
#fill the output file with transformed data

for index, row in df_merge2.iterrows():
    if str(row['end_valid_time'])>= "2000-01-01" or pd.isnull(row['end_valid_time']):
        if row['start_valid_time'] == row['end_valid_time']:
            # convert a date into year-month
            m = convert_date_yearmonth(row['start_valid_time'])
            #replace valid_time
            add_row_ss_item_inter(output_file,row,m)
            #print("end_valid_time==start_valid_time")
        elif pd.isnull(row['end_valid_time']) == True :
            date = convert_date_yearmonth(row['start_valid_time'])
            #end_valid_time==Null means that the rest of the time the entity is valid
            list_month = [m for m in get_list_months_dataset() if m >= date]
            for m in list_month:
                #replace valid_time
                add_row_ss_item_inter(output_file,row,m)
                #print("end_valid_time==null")
        elif row['start_valid_time'] != row['end_valid_time'] and pd.isnull(row['end_valid_time']) == False :
            list_month = [m for m in get_list_months(row['start_valid_time'], row['end_valid_time']) if m in get_list_months_dataset()]
            for m in list_month:
                add_row_ss_item_inter(output_file,row,m)
                #print("end_valid_time=!start_valid_time and end_valid_time not null")

time_end=time.time()
time_run = time_end-time_start
print("It takes " + str(time_run)+ " to transform insert ss_item!")




# Final table of ss_item
# get store_sk and item_sk by merging them with new store and item
#read clean data
input_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/ss_item_inter.csv'
output_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/ss_item.csv'
item_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/item.csv'
store_clean_file = 'D:/SAC 2021/Data/CLEAN/1GB/store.csv'


#read the input file as a dataframe
df_ss_item_clean = pd.read_table(input_clean_file, sep=",")

# Read item
df_item_clean = pd.read_table(item_clean_file, sep=",")
df_item_clean.rename(columns={'item_sk:ID(Item)':'item_sk'}, inplace=True)
df_item_clean=df_item_clean[['item_sk', 'item_id', 'valid_time']].copy()

# merge ss_item and item to get item_sk
df_merge1 = df_ss_item_clean.merge(df_item_clean, on=["item_id","valid_time"], how='left')

# some merges with item_id and valid_time do not exist
df_merge1 = df_merge1[df_merge1['item_sk_y'].notna()]
df_merge1.dtypes
df_merge1["item_sk_y"] = df_merge1["item_sk_y"].astype(np.int64)
df_merge1[':END_ID(Item)'] = df_merge1['item_sk_y']
df_merge1=df_merge1[[':START_ID(Store)', 'store_sk', 'store_id', 'item_id',
       'net_profit', 'quantity', 'wholesale', 'sales_price', 'valid_time',
       ':END_ID(Item)', ':TYPE', 'item_sk_y']].copy()


df_merge1.rename(columns={'item_sk_y':'item_sk'}, inplace=True)


# Read store
df_store_clean = pd.read_table(store_clean_file, sep=",")
df_store_clean.rename(columns={'store_sk:ID(Store)':'store_sk'}, inplace=True)
df_store_clean=df_store_clean[['store_sk', 'store_id', 'valid_time']].copy()
df_merge2 = df_merge1.merge(df_store_clean, on=["store_id","valid_time"], how='left')
df_merge2[':START_ID(Store)'] = df_merge2['store_sk_y']
df_merge2=df_merge2[[':START_ID(Store)', 'store_id', 'item_id', 'net_profit',
       'quantity', 'wholesale', 'sales_price', 'valid_time', ':END_ID(Item)',
       ':TYPE', 'item_sk', 'store_sk_y']].copy()
df_merge2.rename(columns={'store_sk_y':'store_sk'}, inplace=True)

# Delete store_id and item_id
df_ss_item_final = df_merge2[[':START_ID(Store)', 'net_profit', 'quantity',
       'wholesale', 'sales_price', 'valid_time', ':END_ID(Item)', ':TYPE',
       'item_sk', 'store_sk']]
df_ss_item_final.to_csv(output_clean_file, header=True, index=False, sep=',')

