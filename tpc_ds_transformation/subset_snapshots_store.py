
###Extract the data  subset starting from the year 2000 to 2005
#create snapshots of store
#modify the input_file and output_file variable for each transformation.

import pandas as pd
import datetime as dt
import time


# list of month between two dates
def get_list_months(start_date, end_date):
    """

    :param start_date: the start valid time
    :param end_date: the end valid time
    :return: the list of month between the two times. We do not include the end valid time in this list because it is the valid time of the next instance.
    """
    return pd.date_range(start_date, end_date, freq='M').strftime('%Y-%m').tolist()


# + pd.offsets.MonthEnd() to include the end_date https://stackoverflow.com/questions/37890391/how-to-include-end-date-in-pandas-date-range-method

def convert_date_yearmonth(date):
    """

    :param date: must be in date format
    :return: date in string
    """
    res = dt.datetime.strftime(date, '%Y-%m')
    return res


def get_list_months_dataset():  # 10 years of data
    # 1997-03-13: minimum start_valid_time
    # 2007-03-04: maximum
    # choice of 5 years = 72 months
    return pd.date_range("2000-01-01", "2006-01-01", freq='M').strftime('%Y-%m').tolist()


def create_csv_store_clean(output_file):
    """
    :param output_file: the path of the output file
    :return: return the csv into a dataframe form
    """
    dfResult = pd.DataFrame(columns=['store_sk:ID(Store)', 'store_id', 'valid_time',
                                     'date_sk', 'store_name', 'number_employees', 'floor_space', 'hours',
                                     'manager', 'market_id', 'market_desc', 'market_manager',
                                     'street_number', 'street_name', 'street_type', 'suite_number', 'city',
                                     'county', 'state', 'zip', 'country', 'gmt_offset', 'tax_percentage',
                                     ':LABEL'])
    dfResult.to_csv(output_file, index=False, sep=',')
    return dfResult


def add_row_store_clean(output_file, sk, row, new_date):
    """
    :param output_file: csv file that record the transformed data => store_clean.csv
    :param sk: store_sk
    :param row: the row of the input file that is copied to the output file
    :param date: the new valid time of the row
    :return:
    """

    add_line = pd.DataFrame(columns=['store_sk:ID(Store)', 'store_id', 'valid_time',
                                     'date_sk', 'store_name', 'number_employees', 'floor_space', 'hours',
                                     'manager', 'market_id', 'market_desc', 'market_manager',
                                     'street_number', 'street_name', 'street_type', 'suite_number', 'city',
                                     'county', 'state', 'zip', 'country', 'gmt_offset', 'tax_percentage',
                                     ':LABEL'])
    add_line = add_line.append({'store_sk:ID(Store)': sk, 'store_id': row['store_id'], 'valid_time': new_date,
                                'date_sk': row['date_sk'], 'store_name': row['store_name'],
                                'number_employees': row['number_employees'],
                                'floor_space': row['floor_space'], 'hours': row['hours'], 'manager': row['manager'],
                                'market_id': row['market_id'], 'market_desc': row['market_desc'],
                                'market_manager': row['market_manager'],
                                'street_number': row['street_number'], 'street_name': row['street_name'],
                                'street_type': row['street_type'],
                                'suite_number': row['suite_number'], 'city': row['city'], 'county': row['county'],
                                'state': row['state'], 'zip': row['zip'], 'country': row['country'],
                                'gmt_offset': row['gmt_offset'],
                                'tax_percentage': row['tax_percentage'], ':LABEL': row[':LABEL']}, ignore_index=True)
    add_line.to_csv(output_file, mode='a', header=False, index=False,
                    sep=',')  # mode='a' means append, header=False means not copy the header at each addition of a row, index=False means not add automatically an index to each row



#read raw data
input_file = 'D:/SAC 2021/Data/RAW/1GB/store.csv'
output_file = 'D:/SAC 2021/Data/CLEAN/1GB/store.csv'



#read the input file as a dataframe
df_input = pd.read_table(input_file, sep=",",  parse_dates=["start_valid_time", "end_valid_time"])
print(df_input.columns)

#create the output file
df_output=create_csv_store_clean(output_file)
print(df_output.columns)

time_start = time.time()
# fill the output file with transformed data
sk = 0
for index, row in df_input.iterrows():
    if str(row['end_valid_time']) >= "2000-01-01" or pd.isnull(row['end_valid_time']):
        if row['start_valid_time'] == row['end_valid_time']:
            sk = sk + 1
            # convert a date into year-month
            m = convert_date_yearmonth(row['start_valid_time'])
            # replace valid_time
            add_row_store_clean(output_file, sk, row, m)
            print("end_valid_time=start_valid_time")
        elif pd.isnull(row['end_valid_time']) == True:
            date = convert_date_yearmonth(row['start_valid_time'])
            # end_valid_time==Null means that the rest of the time the entity is valid
            list_month = [m for m in get_list_months_dataset() if m >= date]
            for m in list_month:
                sk = sk + 1
                # replace valid_time
                add_row_store_clean(output_file, sk, row, m)
                print("end_valid_time=null")
        elif row['start_valid_time'] != row['end_valid_time'] and pd.isnull(row['end_valid_time']) == False:
            list_month = [m for m in get_list_months(row['start_valid_time'], row['end_valid_time']) if
                          m in get_list_months_dataset()]
            for m in list_month:
                sk = sk + 1
                add_row_store_clean(output_file, sk, row, m)
                print("start_valid_time <> end_valid_time and end_valid <> null")
                # print(row)

time_end = time.time()
time_run = time_end - time_start
print("It takes " + str(time_run) + " to transform insert valid_time!")

