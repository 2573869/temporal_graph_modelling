#Before extracting the subset of data between the year 2000-2005, we made an additional modification on the file has_household_demographics.csv for each dataset due to an error we made.




import pandas as pd


#read raw data
input_file = 'D:/SAC 2021/Has_Household_Demo/RAW/has_household_demographics.csv'
hdemo_file = 'D:/SAC 2021/Has_Household_Demo/RAW/household_demographics.csv'
output_file = 'D:/SAC 2021/Has_Household_Demo/16GB/has_household_demographics.csv'

#read has_household_demographics.csv
df_hhd = pd.read_table(input_file, sep=",")
#rename ':END_ID(Household_Demographics)'
df_hhd.rename(columns={':END_ID(Household_Demographics)':':END_ID(Income_Band)'}, inplace=True)
print(df_hhd.head())
#read household_demographics.csv
df_hd =pd.read_table(hdemo_file, sep=",")
#rename 'hd_demo_sk:ID(Household_Demographics)' to merge with hhd
df_hd.rename(columns={'hd_demo_sk:ID(Household_Demographics)':'current_hdemo_sk'}, inplace=True)
print(df_hd.head())
#only select some columns of household_demographics
df_hd=df_hd[['current_hdemo_sk', 'income_band_sk']].copy()
print(df_hd.head())
df_merge = df_hhd.merge(df_hd, on="current_hdemo_sk", how='left')

print(df_merge.head())
df_merge[':END_ID(Income_Band)'] = df_merge['income_band_sk']
print(df_merge.head())
df_final = df_merge[[':START_ID(Customer)','customer_sk', 'buy_potential','dep_count','vehicle_count', 'start_valid_time', 'end_valid_time',':END_ID(Income_Band)', ':TYPE', 'income_band_sk']]
df_final.head()
df_final.to_csv(output_file, index=False, sep=',')
